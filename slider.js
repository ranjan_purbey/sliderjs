var Slider=function(_parent,_vertical,_r,_g,_b,_width,_init_value,_min,_max,_sameBorderColor,_margin){
var main={
parent:_parent || document.body,
r:_r || 100,
g:_g || 120,
b:_b || 110,
vertical:_vertical,
width:(_width || 80),
min:_min || 0,
max:_max || 1,
value:_init_value,
get margin(){return _margin || ~~(100-main.width)/2},
get borderColor(){
return 'rgba('+((_sameBorderColor)?(main.r+','+main.g+','+main.b):((255-main.b)+','+main.r+','+main.g)) +',1.0)'},
tX:0,
container:null,
slider:null,
inner:null,
defaultStyle:null,
init:function(){
var changedEvt=document.createEvent('Event');
changedEvt.initEvent('changed',true,true);
var movedEvt=document.createEvent('Event');
movedEvt.initEvent('moved',true,true);
main.container=document.createElement('div');
main.slider=document.createElement('div');
main.inner=document.createElement('div');
main.parent.appendChild(main.container);
main.container.appendChild(main.slider);
main.slider.appendChild(main.inner);
main.defaultStyle={
get container(){return ((main.vertical)?("width:0;height:"):("height:0px;width:"))+main.width+"%;border:solid 1px rgb("+main.r+','+main.g+','+main.b+');'+((!main.vertical)?("margin:15px "+main.margin+'%'):('margin:15px'))+';display:inline-block'
},
get slider(){
return "width:20px;height:20px;"+((main.vertical)?"margin:0px -12":"margin:-12px 0")+"px;position:absolute;border-radius:1000px;border:solid 2px "+main.borderColor
},
inner:'width:50%;height:50%;border-radius:1000px;margin:25%;background:rgba(0,0,0,0)'
};
main.container.style.cssText=main.defaultStyle.container;
main.slider.style.cssText=main.defaultStyle.slider;
main.inner.style.cssText=main.defaultStyle.inner;
main.container.getValue=function(){
return main.value;
};
main.container.setValue=function(val){
if(val<main.min)val=main.min;
else if(val>main.max)val=main.max;
main.value=+val;
main.update(val);
main.slider.dispatchEvent(changedEvt);
};
main.container.setValue(main.value||main.min);
//*******************************
window.addEventListener('resize',function(){
main.container.setValue(main.value);
},false);
//****************************
main.slider.addEventListener('touchstart',function(e){
e.preventDefault();
main.slider.style.background=main.borderColor.replace(/\d\.\d/,'0.4');
main.inner.style.background=main.borderColor.replace(/\d\.\d/,'0.4');
},false);
//****************************
main.slider.addEventListener('touchmove',function(e){
e.preventDefault();
if(main.vertical)main.container.setValue(((-e.changedTouches[0].pageY+main.container.offsetTop)/main.container.clientHeight)+1);
else main.container.setValue((e.changedTouches[0].pageX-main.container.offsetLeft)/main.container.clientWidth);

main.slider.dispatchEvent(movedEvt);
},false);

main.slider.addEventListener('touchend',function(e){
main.slider.style.background='rgba(0,0,0,0)';
main.inner.style.background="rgba(0,0,0,0)";
},false);

},
update:function(val){
if(main.vertical){
main.tX=(1-val)*main.container.clientHeight+main.container.offsetTop;
main.slider.style.top=main.tX-main.slider.clientWidth*0.5+'px';
}
else{
main.tX=val*main.container.clientWidth+main.container.offsetLeft;
main.slider.style.left=main.tX-main.slider.clientWidth*0.5+'px';
}
}
};
main.init();
return main.container;
};