﻿#########################
// 	SliderJS	//
#########################

SliderJS aims to arm the web-devs with the functionality of HTML5's range input type in old browsers like android 2.3, using JavaScript.

Live Demo: http://jokr.3owl.com/SliderJS.html

Features:
* Open sourced
* Touch compatible
* Android JellyBean look 'n' feel
* Customizable
* Easy to watch for change in value
* Uses standard web technologies, no proprietary tags.

To-do's:
* Add support for mouse and keyboard (Currently only compatible with touch devices)
* Ability to act as a polyfill.

HowTo:
# Append following in your document's head:
<script src='slider.js'></script>
# To insert a slider, use following code:
var mySlider=new Slider(parent,vertical,r,g,b,width,initial_value,min_value,max_value,same border color,margin);

...where:
parent - the container element to which to append the slider (default:document.body)
vertical - (true/1 or false/0) whether to make the slider vertical(default:false)
r,g,b - (integers b/w 0-255) the color of the slider(bar)
width - (number b/w 0-100) percentage width( or height) of slider(default:80)
initial_value/min_value/max_value - (number b/w 0-1) the initial/minimum/maximum values of the slider (default:0/0/1)

same_border_color - (0 or 1) whether to use the same color for disk as the bar(default:0)
margin - the horizontal margin b/w slider and its parent (by default the slider is placed in center, no effect in vertical mode)

Methods:
getValue() -returns the value of the slider (a number b/w 0-1)
setValue(val) - sets the value of the slider to 'val'.

Events:
changed :triggered whenever the value of the slider changes.
moved :triggered when the slider's disc is moved (the need for this event arose when I was trying to sync the value of a slider with the current time of an audio element).

Notes !important :
> Since the width/height of the slider is set as percentage of its parents properties, you must explicitly specify the height of at least one parent in the DOM hierarchy in pixels(px) when using vertical mode.
> Auto-resizing works only in horizontal mode.
> When using slider inside a table, wrap it with a div/span whose 'display' property is specified.
> If you are trying to sync the value of slider with the currentTime of an audio element like:
 audio.timeupdate-->slider.Value(audio.currentTime/audio.duration)
 slider.changed --> audio.currentTime=slider.getValue()
 
...it will cause an infinite loop, because every time setValue() method is called, slider's value changes. In this case you can watch for 'moved' event instead of 'changed'.